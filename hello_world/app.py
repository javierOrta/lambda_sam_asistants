import json
from pymongo import MongoClient
from pandas.io.json import json_normalize
import pandas as pd
from functools import reduce

# import requests


def clean_dict(dict):
    del dict["_id"]
    return dict


def array_agrupados(usuarios,data_list,tag_id,tag_total,tag_valuesInTime,tag_total_etiqueta):
    l = []
    for user in usuarios:
        v = {}
        v[tag_id] = user
        aux = data_list[data_list['Usuario'] == user][['date', tag_total]]
        v[tag_valuesInTime] = list(map(lambda x, y: {'date': x, 'labels': y}, aux['date'], aux[tag_total]))
        v[tag_total_etiqueta] = reduce(lambda a, b: a + b, aux[tag_total].to_list())

        l.append(v)
    data = l[:]
    return data


def output_agregados():
    myclient = MongoClient("mongodb://gestalabs:gl2009_!@34.218.245.142/labelme_data?authSource=admin")

    mydb = myclient["labelme_data"]

    mycol = mydb["labelme_stadistics"]

    cursorData = list(mycol.find({}, {"DATETIME": 1, "USER_NAME": 1, "IMAGE_DATA": 1}))

    lambda_clean=list(map(lambda x:clean_dict(x),cursorData))

    dfAsistantData =  json_normalize(lambda_clean)

    dfAsistantData = dfAsistantData.drop('IMAGE_DATA.IMAGE', axis=1)

    dfAsistantData = dfAsistantData.drop('IMAGE_DATA', axis=1)

    start_date = '2019/02/08'
    end_date = '2019/02/28 23:59:00'

    dfAsistantData['DATETIME'] = pd.to_datetime(dfAsistantData['DATETIME'])

    mask = (dfAsistantData['DATETIME'] >= start_date) & (dfAsistantData['DATETIME'] <= end_date)

    dfAsistantData = dfAsistantData.loc[mask]

    dfAsistantData.dropna(how='any', inplace=True)

    avgPointsValue = dfAsistantData['IMAGE_DATA.TOTAL_POINTS'].mean()
    avgLabelsValue = dfAsistantData['IMAGE_DATA.TOTAL_LABELS'].mean()

    dfAsistantData[['date', 'time']] = dfAsistantData.DATETIME.astype(str).str.split(" ", expand=True)
    dfAsistantData = dfAsistantData.drop('DATETIME', axis=1)#print(dfAsistantData)
    #dfAsistantData['date'] = pd.to_datetime(dfAsistantData['date'])

    usuarios_date_total_labels = dfAsistantData.groupby(['USER_NAME','date'])['IMAGE_DATA.TOTAL_LABELS'].sum().to_frame()
    usuarios_date_total_labels.reset_index(inplace=True)

    usuarios_date_total_points = dfAsistantData.groupby(['USER_NAME','date'])['IMAGE_DATA.TOTAL_POINTS'].sum().to_frame()
    usuarios_date_total_points.reset_index(inplace=True)

    usuarios_date_total_labels.columns = ['Usuario','date','total_labels']
    usuarios=list(set(usuarios_date_total_labels['Usuario']))

    usuarios_date_total_points.columns = ['Usuario','date','total_point']
    usuarios_points=list(set(usuarios_date_total_points['Usuario']))


    ##Blue's code
    usuarios_date_total_labels_points = pd.merge(usuarios_date_total_labels, usuarios_date_total_points,  how='inner', left_on=['Usuario','date'], right_on = ['Usuario','date'])
    usuarios_date_total_labels_points.reset_index(inplace=True)
    usuarios_date_total_labels_points['avg']=round(usuarios_date_total_labels_points['total_point']/usuarios_date_total_labels_points['total_labels'],2)

    data_agrup_labels = array_agrupados(usuarios,usuarios_date_total_labels_points,"assistantId",'total_labels',"valuesInTimeLabels","totalLabels")

    data_agrup_points = array_agrupados(usuarios_points,usuarios_date_total_labels_points,"assistantId",'total_point',"valuesInTimePoints",'totalPoints')

    data_agrup_avg = array_agrupados(usuarios_points,usuarios_date_total_labels_points,"assistantId",'avg',"valuesInTimeAvg",'avg')

    df1 = pd.DataFrame(data_agrup_labels)
    df2 = pd.DataFrame(data_agrup_points)
    df3 = pd.DataFrame(data_agrup_avg)

    df_join = pd.merge(df1, df2, on='assistantId', how='inner')
    df_join2 = pd.merge(df_join, df3, on='assistantId', how='inner')

    userValues_temp =[]
    for index, row in df_join2.iterrows():

        temp_json_value = {
                "assistantId": row['assistantId'],
                "totalPoints": row['totalPoints'],
                "totalLabels": row['totalLabels'],
                "totalPoints_Labels": row['avg'],
                "valuesInTimePoints":row['valuesInTimePoints'],
                "valuesInTimeLabels": row['valuesInTimeLabels'],
                "valuesInTimePointsLabels": row['valuesInTimeAvg'],
                "avgDayPoints": int(row['totalPoints'])/len(row['valuesInTimePoints']),
                "avgDayLabels":  int(row['totalLabels'])/len(row['valuesInTimePoints']),
                "avgDayPointsLabels": int(row['avg'])/len(row['valuesInTimePoints'])

            }
        userValues_temp.append(temp_json_value)


    outPut = {
        "avgPoint": round(avgPointsValue),
        "avgLabels": round(avgLabelsValue),
        "userValues": userValues_temp
    }
    return outPut


def lambda_handler(event, context):
    json_res =output_agregados()
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": json_res,
        }),
    }
